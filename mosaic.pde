int numOfTiles = 0; //jumlah tiles maksimum
ArrayList<PImage> tilesArray = new ArrayList<PImage>(); //array of tiles
int tileHeight; int tileWidth; 
int refImgHeight; int refImgWidth;
PImage refImg;
boolean changed, tileSelected, refImgExist, tilesExist, delete;
float max_distance;
String path, imgPath;

void setup() {
  //set size background
  size(600,600);
  
  refImgWidth = 600;
  refImgHeight = 600;
  
  
  //set size tile
  tileHeight = 5;
  tileWidth = 5;
  
  //set newPath for tiles
  imgPath = sketchPath()+"/data";
  
  //set max distance
  //max_distance = dist(0, 0, refImgWidth, refImgHeight);
  
  //check reference image exist
  cekRefImg();
  if(refImgExist==true){
    //check stored tiles
    cekTiles();
  }
  
  
  println("FINISH!");
  
}

void cekRefImg(){
  //cek reference Image pada path data
  String[] refImageName = listTileNames(imgPath+"/");
  int c=0, count=0;
  while(c<refImageName.length){
    if(refImageName[c].equals("mainImage.jpg")){
      count+=1;
    }
    c++;
  }
  
  if(count == 0){
    refImgExist = false;
    selectRefImg();
    println("RefImage DOESNT EXIST");
  }
  else {
    println("RefImage DOES EXIST");
    refImgExist = true;
  }
}

void cekTiles(){
  //cek rerference Image pada path data
  String[] tilesName = listTileNames(imgPath+"/");
      
  int c=0, count=0;
  while(c<tilesName.length){
    
    if(tilesName[c].equals("tile"+count+".jpg")){
      count+=1;
    }
    c++;
    
  }
  
  //update jumlah tiles yang ada
  numOfTiles = count;
  println("CEKTILES: "+numOfTiles);
  
  //if stored tiles null or less than 3
  if(numOfTiles<3 || numOfTiles==0){
    
    tilesExist = false;
    while(numOfTiles<3){
      addTiles();
      numOfTiles+=1;
    }    
   
  }
  if(numOfTiles>=3) tilesExist = true;
  
}

void keyReleased(){
  if(key == CODED) {
    if(keyCode == LEFT){ 
      println("HAPUS SEMUA FILE IMG");
      deleteStoredImg();
      setup();
    }
    else if(keyCode == UP){
      addTiles();
      println("TAMBAH TILE");
    }
    else if(keyCode == DOWN){
      println("SHIFT");
      //selectRefImg();
    }
  }
}
void deleteStoredImg(){
  
  //Hapus semua image yang tersimpan pada path data
  String[] tilesName = listTileNames(imgPath+"/");
  
    int c=0;
    while(c<tilesName.length){
      //File file = new File(imgPath+"/"+"tile"+c+".jpg");
      File file = new File(imgPath+"/"+tilesName[c]);
      file.delete();
      c++;      
    }  
}

//menambah tiles yang sudah ada
void addTiles(){
  selectInput("Select Tiles:", "tileSelected");
}

void tileSelected(File selection){
  
  File newTile;
  if(selection != null){
    
    path = selection.getAbsolutePath();
    selection = new File(path);
    
    String[] filesName = listTileNames(imgPath+"/");
      
    int c=0;
    while(c<filesName.length){
      
      newTile = new File(imgPath+"/"+"tile"+c+".jpg");
      
      selection.renameTo(newTile);
     
      c++;
      
    }

  }
}

//memilih reference image
void selectRefImg(){
  selectInput("Select Main Image:", "refImgSelected");   
}

void refImgSelected(File selection){
  
  File newTile;
  if(selection != null){
    
    path = selection.getAbsolutePath();
    selection = new File(path);
    
    newTile = new File(imgPath+"/"+"mainImage"+".jpg");
    selection.renameTo(newTile);
  }
  
  //check stored tiles
  cekTiles();
}


void draw() {
  
  //load photomosaic setelah diresize
  changeSize();
  if (changed) {
    loadMain();
    changed = false;
  }
  
}

//Fungsi list nama file
String[] listTileNames(String dir){
  //cek nama file di direktori tile
  File tile = new File(dir);
  if(tile.isDirectory()){
    String names[] = tile.list();
    return names;

  }
  else{
    return null;
  }
  
}


ArrayList<File> listFilesRecursive(String dir){
  ArrayList<File> fileList = new ArrayList<File>();
  recurseDir(fileList, dir);
  return fileList;
}

void recurseDir(ArrayList<File> list, String dir){
  File f = new File(dir);
  if(f.isDirectory()){
    list.add(f);
    File[] subfiles = f.listFiles();
    for(int i=0; i<subfiles.length;i++){
      recurseDir(list, subfiles[i].getAbsolutePath());
    }
    
  }
  else{
    list.add(f);
  }
}



//load reference image dan menggantinya dengan tile
void loadMain() {
  tilesArray.clear();
  
  // load reference image
  refImg = loadImage("mainImage.jpg");
  //set maks reference Image
  if(refImg.width > refImg.height){
    refImgWidth = 600;
    refImgHeight = 600-30;
  }
  else if(refImg.width < refImg.height){
    refImgWidth = 600-30;
    refImgHeight = 600;
  }
  
  //update size of background
  this.getSurface().setSize(refImgWidth,refImgHeight);
  
  refImg.resize(refImgWidth, refImgHeight);
  println("refImgWidth "+refImgWidth+ "refImgHeight "+refImgHeight);
  
      
  for (int i = 0; i < numOfTiles; i++) {
    //load tiles
    PImage tiles = loadImage("tile" + i + ".jpg"); 
    tiles.resize(tileWidth, tileHeight); 
      
    //tambah tiles pada array of tiles
    tilesArray.add(tiles);
    tilesArray.add(setBrightness(tiles, 1.5)); //membuat tile menjadi lebih gelap
    tilesArray.add(setBrightness(tiles, .8)); //membuat tile menjadi lebih terang
   }
  
  
  
  //membagi referensi image menjadi beberapa patch yang disesuaikan dengan ukuran tile
  for (int x = 0; x < refImgWidth; x+=tileWidth) {
    for (int y = 0; y < refImgHeight; y+=tileHeight) {
      
      PImage patches = refImg.get(x, y, tileWidth, tileHeight);
      
      //memilih tiles untuk disusun pada patches
      image(getClosest(patches), x, y);
    }
  }
}

//ambil tile untuk dipasang pada patch yg paling kecil selisih (jarak) warnanya 
PImage getClosest(PImage image) {
  //Algoritma
  
  //inisiasi selisih warna patch dengan tile berindeks 0 pada array of tiles
  int minDistVal = colorDistanceOf(image, tilesArray.get(0)); //nilai selisih minimum awal
  
  int closest = 0;
  
  //periksa nilai selisih warna antara patch dengan tiles berindeks > 1 pada array of tiles  
  for (int i = 1 ; i < tilesArray.size() ; i++) {
    
    //inisiasi nilai selisih warna patch dengan tiles berindeks > 1
    int curDistVal = colorDistanceOf(image, tilesArray.get(i));
      
    
    //pilih nilai selisih warna paling kecil
    if (curDistVal < minDistVal) {
      closest = i;
      minDistVal = curDistVal;
    }
    
  }
    
  //ambil tiles dengan selisih warna paling kecil
  return tilesArray.get(closest);
}

//hitung selisih (jarak) antara warna patch dengan warna tile
int colorDistanceOf(PImage i1, PImage i2) {
  float distVal = 0;
  for (int i = 0; i < i1.pixels.length; i++) {
    distVal += dist(red(i1.pixels[i]), green(i1.pixels[i]), blue(i1.pixels[i]), 
    red(i2.pixels[i]), green(i2.pixels[i]), blue(i2.pixels[i]));
  }
  return round(distVal);
}

//mengubah ukuran tiles dengan interaksi mouse 
void changeSize() {
  if (mousePressed && (mouseButton == LEFT)) {
    tileHeight += 5;
    tileWidth += 5;
    changed = true;
  } 
  else if (mousePressed && (mouseButton == RIGHT) && (tileHeight > 5)) {
    tileHeight -= 5;
    tileWidth -= 5;
    changed = true;
  } 
}

//menambah tile dengan modifikasi brightness dari tile yang ada
PImage setBrightness(PImage in, float factor) {
  PImage out = createImage(in.width, in.height, RGB);
  for (int i = 0; i < in.width * in.height; i++) {
    out.pixels[i] = color(red(in.pixels[i])/factor, green(in.pixels[i])/factor, blue(in.pixels[i])/factor);
  }
  return out;
}
